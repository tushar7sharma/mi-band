package com.motioncoding.miband;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothAdapter.LeScanCallback;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import java.util.*;
import java.util.Hashtable;


public class MiActivity extends Activity implements LeScanCallback {

	private BluetoothAdapter mBluetoothAdapter;
	private boolean mScanning;
	private TextView mTextView;
	private Handler mHandler = new Handler();
	// Stops scanning after 10 seconds.
	private static final long SCAN_PERIOD = 15000;
	private LeDeviceListAdapter mLeDeviceListAdapter;

	//Hashtable bands = new Hashtable();
	HashMap <String, Integer> bands = new HashMap<String, Integer>();

	String m1 = "88:0F:10:66:6F:83";
	String m2 = "88:0F:10:66:85:3D";
	String names ="";
	int count =0;
	int number_bands = 2;
	//check.put(m1,3);
	int halt=0,num=0;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getActionBar().hide();
		bands.put(m1, 0);
		bands.put(m2,0);

		setContentView(R.layout.activity_mi);
		mTextView = (TextView) findViewById(R.id.text_search);
		mBluetoothAdapter = ((BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE))
				.getAdapter();
	}

	@Override
	public void onResume() {
		super.onResume();
		scanLeDevice(true);
	}

	@SuppressWarnings("deprecation")
	// L Apis are buggy and crap!
	private void scanLeDevice(final boolean enable) {
		if (enable) {
			mTextView.setText(R.string.looking_for_miband);
			// Stops scanning after a pre-defined scan period.

				mHandler.postDelayed(new Runnable() {

					@Override
					public void run() {
						mScanning = false;
						mBluetoothAdapter.stopLeScan(MiActivity.this);
//					mTextView.setText(R.string.not_found);
						halt = 1;
					}
				}, SCAN_PERIOD);


			mScanning = true;
			mBluetoothAdapter.startLeScan(this);
		} else {
			mScanning = false;
			mBluetoothAdapter.stopLeScan(this);
		}
	}

	@Override
	public void onLeScan(BluetoothDevice device, int rssi, byte[] scanRecord) {
		if (device != null && device.getName() != null
				& device.getName().equals("MI")) {
			System.out.println(device.getAddress());

			scanLeDevice(false); // we only care about one miband so that's
									// enough
			//mTextView.setText(device.getAddress());

			mTextView.setText(names);

			if (halt==0)
			{

				if(count==number_bands)
					halt=1;
				else
				{
				if(bands.get(device.getAddress())==1)
				{
					//scanLeDevice(true);
					mBluetoothAdapter.startLeScan(this);

				}
				else
				{
					count ++;
					bands.put(device.getAddress(),1);
					names = names + "    "+ device.getAddress();
					mTextView.setText(names);
					//scanLeDevice(true);
					mBluetoothAdapter.startLeScan(this);
				}
				}
			}
			else {
				mTextView.setText("chcha");

			}


//			Intent intent = new Intent(getApplicationContext(), MiOverviewActivity.class);
//			intent.putExtra("address", device.getAddress());
//			startActivity(intent);
		}
	}

	@Override
	public void onPause() {
		super.onPause();
		scanLeDevice(false);
	}

	private class LeDeviceListAdapter extends BaseAdapter {
		private ArrayList<BluetoothDevice> mLeDevices;
		private LayoutInflater mInflator;

		public LeDeviceListAdapter() {
			super();
			mLeDevices = new ArrayList<BluetoothDevice>();
			mInflator = MiActivity.this.getLayoutInflater();
		}

		public void addDevice(BluetoothDevice device) {
			if(!mLeDevices.contains(device)) {
				mLeDevices.add(device);
			}
		}

		public BluetoothDevice getDevice(int position) {
			return mLeDevices.get(position);
		}

		public void clear() {
			mLeDevices.clear();
		}

		@Override
		public int getCount() {
			return mLeDevices.size();
		}

		@Override
		public Object getItem(int i) {
			return mLeDevices.get(i);
		}

		@Override
		public long getItemId(int i) {
			return i;
		}

		@Override
		public View getView(int i, View view, ViewGroup viewGroup) {
			ViewHolder viewHolder;
			// General ListView optimization code.
			if (view == null) {
				view = mInflator.inflate(R.layout.listitem_device, null);
				viewHolder = new ViewHolder();
				viewHolder.deviceAddress = (TextView) view.findViewById(R.id.device_address);
				viewHolder.deviceName = (TextView) view.findViewById(R.id.device_name);
				view.setTag(viewHolder);
			} else {
				viewHolder = (ViewHolder) view.getTag();
			}

			BluetoothDevice device = mLeDevices.get(i);
			final String deviceName = device.getName();
			if (deviceName != null && deviceName.length() > 0)
				viewHolder.deviceName.setText(deviceName);
			else
				viewHolder.deviceName.setText("helllo");
				//viewHolder.deviceName.setText(R.string.unknown_device);
			viewHolder.deviceAddress.setText(device.getAddress());

			return view;
		}
	}

	static class ViewHolder {
		TextView deviceName;
		TextView deviceAddress;
	}

}

